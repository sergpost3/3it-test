<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "authors".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 */
class Authors extends \yii\db\ActiveRecord
{
    public $author;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'authors';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [ [ 'firstname', 'lastname' ], 'required' ],
            [ [ 'firstname', 'lastname' ], 'string', 'max' => 50 ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
        ];
    }

    public static function get_select() {
        $res = [ ];
        foreach( self::find()->asArray()->all() as $value )
            $res[$value["id"]] = $value["firstname"] . " " . $value["lastname"];
        return $res;
    }
}
