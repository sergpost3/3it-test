<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Books;

/**
 * BooksSearch represents the model behind the search form about `app\models\Books`.
 */
class BooksSearch extends Books
{
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [ [ 'author_id' ], 'integer' ],
            [ [ 'name', 'date' ], 'safe' ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search( $params ) {
        $query = Books::find()
            ->select( [ "books.*", "CONCAT(authors.firstname, ' ', authors.lastname) as author" ] )
            ->leftJoin( "authors", "authors.id = books.author_id" );

        $dataProvider = new ActiveDataProvider( [
            'query' => $query,
        ] );

        $this->load( $params );

        if( !$this->validate() ) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if( $this->date ) {
            $date = array_map( "trim", explode( " - ", trim( $this->date ) ) );
            $query->andFilterWhere( [ '>=', 'date', $date[0] ] )
                ->andFilterWhere( [ '<=', 'date', $date[1] ] );
        }

        $query->andFilterWhere( [
            'date' => $this->date,
            'author_id' => $this->author_id,
        ] );

        $query->andFilterWhere( [ 'like', 'name', $this->name ] );

        return $dataProvider;
    }
}
