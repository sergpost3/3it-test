<?php

namespace app\models;

use app\models\Users;

class User extends \yii\base\Object implements \yii\web\IdentityInterface
{
    public $id;
    public $login;
    public $email;
    public $created;
    public $password;
    public $authKey;
    public $accessToken;

    /**
     * @inheritdoc
     */
    public static function findIdentity( $id ) {
        return isset( self::$users[$id] ) ? new static( self::$users[$id] ) : null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken( $token, $type = null ) {
        foreach( self::$users as $user ) {
            if( $user['accessToken'] === $token ) {
                return new static( $user );
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param  string $username
     * @return static|null
     */
    public static function findByUsername( $username ) {
        /*foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }*/
        $users = Users::find()
            ->where( [ "login" => $username ] );

        if( $users->count() == 0 )
            return null;
        else
            return new static( $users->one() );
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey( $authKey ) {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword( $password ) {
        return $this->password === md5( md5( $password ) );
    }
}
