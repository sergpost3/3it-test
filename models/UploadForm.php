<?php
namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $preview;

    public function rules() {
        return [
            [ [ 'preview' ], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg' ],
        ];
    }

    public function upload() {
        if( $this->validate() ) {
            $adr = 'web/images/' . $this->preview->baseName . '.' . $this->preview->extension;
            $this->preview->saveAs( $adr );
            return $adr;
        }
        else {
            return false;
        }
    }
}