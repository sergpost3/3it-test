<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;
use app\models\UploadForm;

/**
 * This is the model class for table "books".
 *
 * @property integer $id
 * @property string $name
 * @property integer $date_create
 * @property integer $date_update
 * @property string $preview
 * @property integer $date
 * @property integer $author_id
 */
class Books extends \yii\db\ActiveRecord
{
    public $author;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'books';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [ [ 'name', 'date_create', 'date_update', 'date', 'author_id' ], 'required' ],
            //[ [ 'date_create', 'date_update', 'date' ], 'date' ],
            [ [ 'author_id' ], 'integer' ],
            [ [ 'preview' ], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg' ],
            [ [ 'name' ], 'string', 'max' => 100 ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'date_create' => 'Date Create',
            'date_update' => 'Date Update',
            'preview' => 'Preview',
            'date' => 'Date',
            'author_id' => 'Author ID',
        ];
    }

    public function _save() {
        $model = new UploadForm();

        if( Yii::$app->request->isPost ) {
            $model->preview = UploadedFile::getInstance( $this, 'preview' );
            if( $model->preview )
                $this->preview = $model->upload();
        }
        $this->date_update = date( "Y-m-d" );
        return $this->save();
    }

    public function create() {
        $this->date_create = date( "Y-m-d" );
        return $this->_save();
    }
}
