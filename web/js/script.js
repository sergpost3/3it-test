$(document).ready(function() {
    $("a[aria-label=View]").click(function() {
        event.preventDefault();
        //$.get("/books/view?id=1", function(data) {
        $.get($(this).attr("href"), function(data) {
            $('.modal-body').html(data);
            $('#my-modal').modal({show:true});
        });
        return false;
    })
});