<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Users;
use app\models\Cards;
use app\models\ContactForm;

class CardController extends Controller
{
    public function actionAdd() {
        $model = new Cards();
        if( $model->load( Yii::$app->request->post() ) && $model->_save() ) {
            return $this->redirect( "/" );
        }

        return $this->render( "edit", [ "model" => $model, "title" => "New card" ] );
    }

    public function actionEdit() {
        if( $post = Yii::$app->request->post() ) {
            $model = Cards::find()
                ->where( [ "id" => $post["Cards"]["id"] ] )
                ->one();
            if( $model->user != Yii::$app->user->identity->id )
                return $this->redirect( "/" );
            if( $model->load( $post ) && $model->_save() ) {
                return $this->redirect( "/" );
            }
        }

        if( !$model ) {
            $model = Cards::find()
                ->where( [ "id" => Yii::$app->request->get( "card_id" ) ] );
            if( $model->count() == 0 )
                return $this->redirect( "/" );
            $model = $model->one();
            if( $model->user != Yii::$app->user->identity->id )
                return $this->redirect( "/" );
        }

        return $this->render( "edit", [ "model" => $model, "title" => "Edit card" ] );
    }
}