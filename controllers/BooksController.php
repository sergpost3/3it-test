<?php

namespace app\controllers;

use Yii;
use app\models\Books;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\BooksSearch;

use app\models\Authors;

/**
 * BooksController implements the CRUD actions for Books model.
 */
class BooksController extends Controller
{
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => [ 'post' ],
                ],
            ],
        ];
    }

    /**
     * Lists all Books models.
     * @return mixed
     */
    public function actionIndex() {
        if( Yii::$app->user->isGuest )
            return $this->redirect( '/' );
        $searchModel = new BooksSearch();
        $dataProvider = $searchModel->search( Yii::$app->request->queryParams );

        return $this->render( 'index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ] );
    }

    /**
     * Displays a single Books model.
     * @param integer $id
     * @return mixed
     */
    public function actionView( $id ) {
        if( Yii::$app->user->isGuest )
            return $this->redirect( '/' );
        return $this->renderPartial( 'view', [
            'model' => $this->findModel( $id ),
        ] );
    }

    /**
     * Creates a new Books model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        if( Yii::$app->user->isGuest )
            return $this->redirect( '/' );
        $model = new Books();

        if( $model->load( Yii::$app->request->post() ) && $model->create() ) {
            return $this->redirect( '/books' );
        }
        else {
            print_r( $model );
            return $this->render( 'create', [
                'model' => $model,
            ] );
        }
    }

    /**
     * Updates an existing Books model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate( $id ) {
        if( Yii::$app->user->isGuest )
            return $this->redirect( '/' );
        $model = $this->findModel( $id );

        if( $model->load( Yii::$app->request->post() ) && $model->_save() ) {
            return $this->redirect( '/books' );
        }
        else {
            return $this->render( 'update', [
                'model' => $model,
            ] );
        }
    }

    /**
     * Deletes an existing Books model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete( $id ) {
        if( Yii::$app->user->isGuest )
            return $this->redirect( '/' );
        $this->findModel( $id )->delete();

        return $this->redirect( [ 'index' ] );
    }

    /**
     * Finds the Books model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Books the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel( $id ) {
        if( ( $model = Books::findOne( $id ) ) !== null ) {
            return $model;
        }
        else {
            throw new NotFoundHttpException( 'The requested page does not exist.' );
        }
    }
}
