<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="body-content">
        <h1>
            <?php if( Yii::$app->user->isGuest ) : ?>
                Sign in or login please
            <?php else : ?>
                <a href="/books/">Go to books list</a>
            <?php endif; ?>
        </h1>
    </div>
</div>
