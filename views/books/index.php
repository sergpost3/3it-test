<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use app\models\Authors;
use kartik\daterange\DateRangePicker;
use branchonline\lightbox\Lightbox;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Books';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="books-index">

    <h1><?= Html::encode( $this->title ) ?></h1>

    <p>
        <?= Html::a( 'Create Books', [ 'create' ], [ 'class' => 'btn btn-success' ] ) ?>
    </p>

    <?= GridView::widget( [
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'name',
            [
                'label' => 'Preview',
                'format' => "html",
                'value' => function ( $data ) {
                    return Html::img( "/" . $data->preview, [ 'class' => 'img_small' ] );
                },
            ],
            [
                'attribute' => 'author',
                'filter' => Html::activeDropDownList( $searchModel, 'author_id', Authors::get_select(), [ 'class' => 'form-control', 'prompt' => 'Select Author' ] ),
            ],
            [
                'attribute' => 'date',
                'filter' => DateRangePicker::widget( [
                    'name' => 'BooksSearch[date]',
                    'value' => ( isset( $_GET["BooksSearch[date]"] ) ) ? $_GET["BooksSearch[date]"] : '',
                    'convertFormat' => false,
                    'pluginOptions' => [
                        'format' => 'YYYY-MM-DD',
                        'opens' => 'left',
                    ]
                ] ),
            ],
            'date_create',
            //'date_update',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {view} {delete}',
                'buttons' => [
                    'update' => function ( $url, $model, $id ) {
                        return '<a href="' . Url::to( [ 'update', 'id' => $id ] ) . '" title="Update" aria-label="Update" data-pjax="0" target="_blank"><span class="glyphicon glyphicon-pencil"></span></a>';
                    }
                ]
            ],
        ],
    ] ); ?>

</div>
<div class="modal fade" id="my-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body"></div>
        </div>
    </div>
</div>