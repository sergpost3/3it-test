<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Authors;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Books */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="books-form">

    <?php $form = ActiveForm::begin( [ 'options' => [ 'enctype' => 'multipart/form-data' ] ] ); ?>

    <?= $form->field( $model, 'name' )->textInput( [ 'maxlength' => true ] ) ?>

    <?= $form->field( $model, 'preview' )->fileInput() ?>

    <?= $form->field( $model, 'date' )->widget( DatePicker::className(), [ 'dateFormat' => 'yyyy-MM-dd' ] ) ?>

    <?= $form->field( $model, 'author_id' )->dropDownList( Authors::get_select() )->label( "Author" ) ?>


    <div class="form-group">
        <?= Html::submitButton( $model->isNewRecord ? 'Create' : 'Update', [ 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary' ] ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
